# Gerador de Senha
Um simples gerador de senha que permite aos usuários selecionar diferentes critérios, como letras maiúsculas, minúsculas, números e caracteres especiais, para gerar uma senha forte.

## Funcionalidades
- Permitir aos usuários selecionar critérios para gerar a senha.
- Gerar uma senha forte com base nos critérios selecionados.
- Copiar a senha gerada para a área de transferência.

## Tecnologias Utilizadas
- HTML
- CSS (com Tailwind CSS para estilos)
- JavaScript

## Como Usar
1. Faça o download ou clone este repositório.
2. Abra o arquivo `index.html` em um navegador da web.
3. Selecione os critérios desejados para a senha.
4. Clique no botão "Gerar Senha" para gerar a senha.
5. Se desejar, clique no botão "Copiar" para copiar a senha para a área de transferência.

## Demonstração
Uma demonstração deste projeto está disponível [aqui](https://gerador-senha-drab.vercel.app/).

## Créditos
Este projeto foi criado como um exemplo didático para demonstrar o uso de HTML, CSS e JavaScript na criação de uma aplicação web simples.

## Contribuições

Contribuições são bem-vindas! Sinta-se à vontade para enviar um pull request com melhorias ou correções de bugs.

## Autor
Allan Possani da Cruz (allanpcruz@gmail.com) - Desenvolvedor Principal

## Licença
Este projeto está licenciado sob a [Licença MIT](LICENSE).
