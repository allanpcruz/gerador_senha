window.addEventListener('DOMContentLoaded', validarOpcoes);

document.querySelector('#gerarSenhaBtn').addEventListener('click', validarOpcoes);
document.querySelector('#senhaLength').addEventListener('input', atualizarOutput);
document.querySelector('#copiarSenhaBtn').addEventListener('click', copiarSenha);
document.querySelector('#chkMaiusculas').addEventListener('click', validarOpcoes);
document.querySelector('#chkMinusculas').addEventListener('click', validarOpcoes);
document.querySelector('#chkNumeros').addEventListener('click', validarOpcoes);
document.querySelector('#chkEspeciais').addEventListener('click', validarOpcoes);

const maiusculas = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
const minusculas = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];
const numeros = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
const especiais = ['!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '_', '+', '{', '}', '[', ']', '|', ':', ';', '"', '<', '>', ',', '.', '?', '/'];

const output = document.querySelector('output[for="senhaLength"]');
const inputSenha = document.querySelector('#senha');

let utilizarMaiusculas;
let utilizarMinusculas;
let utilizarNumeros;
let utilizarEspeciais;

function gerarSenha() {
    const tamanho = document.querySelector('#senhaLength').value;

    let caracteres = [];

    if (utilizarMaiusculas) {
        caracteres = caracteres.concat(maiusculas);
    }

    if (utilizarMinusculas) {
        caracteres = caracteres.concat(minusculas);
    }

    if (utilizarNumeros) {
        caracteres = caracteres.concat(numeros);
    }

    if (utilizarEspeciais) {
        caracteres = caracteres.concat(especiais);
    }

    let senha = '';

    for (let i = 0; i < tamanho; i++) {
        const randomIndex = Math.floor(Math.random() * caracteres.length);
        senha += caracteres[randomIndex];
    }

    inputSenha.value = senha;

};


function atualizarOutput() {
    output.textContent = this.value;
    validarOpcoes();
}

function validarOpcoes() {
    utilizarMaiusculas = document.querySelector('#chkMaiusculas').checked;
    utilizarMinusculas = document.querySelector('#chkMinusculas').checked;
    utilizarNumeros = document.querySelector('#chkNumeros').checked;
    utilizarEspeciais = document.querySelector('#chkEspeciais').checked;

    if (!utilizarMaiusculas && !utilizarMinusculas && !utilizarNumeros && !utilizarEspeciais) {
        document.querySelector('#msgErro').innerText = 'Selecione ao menos uma opção acima';
        destacarCheckboxes();
        inputSenha.value = '';
        return;
    }

    document.querySelector('#msgErro').innerText = '';
    removerDestaqueCheckboxes();

    gerarSenha();
}

function destacarCheckboxes() {
    document.querySelector('#opcoes').classList.add('border-red-500');
}

function removerDestaqueCheckboxes() {
    document.querySelector('#opcoes').classList.remove('border-red-500');
}

function copiarSenha() {
    const senhaInput = document.querySelector('#senha');
    senhaInput.select();
    senhaInput.setSelectionRange(0, 99999);
    navigator.clipboard.writeText(senhaInput.value);
    window.getSelection().removeAllRanges();
}
